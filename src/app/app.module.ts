import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,  FormGroup,  FormControl, ReactiveFormsModule } from '@angular/forms';

import { Http, Headers, HttpModule, RequestOptions } from '@angular/http';
import { Routes, RouterModule, RouterLinkActive } from '@angular/router';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { HttpClientModule} from '@angular/common/http';
import { CdkTableModule} from '@angular/cdk/table';
// import * as FusionCharts from 'fusioncharts';
// import * as Charts from 'fusioncharts/fusioncharts.charts';
// import * as FintTheme from 'fusioncharts/themes/fusioncharts.theme.fint';
// import { FusionChartsModule } from 'angular4-fusioncharts';
import { AgmCoreModule } from '@agm/core';

import { DynamicFormComponent }         from './home/dynamic-form.component';
import { DynamicFormQuestionComponent } from './home/dynamic-form-question.component';
import { AgmSnazzyInfoWindowModule } from '@agm/snazzy-info-window';

// FusionChartsModule.fcRoot(FusionCharts, Charts, FintTheme);
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';

import { AppRoute } from './app.route';
import { AppComponent } from './app.component';
import { HeaderComponent } from './admin/header/header.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { FooterComponent } from './admin/footer/footer.component';
import { AdminComponent } from './admin/admin.component';
import { AccountSettingComponent } from './admin/account-setting/account-setting.component';
import { HomeComponent } from './home/home.component';
import { TermsComponent } from './terms/terms.component';
import { MailboxComponent } from './admin/mailbox/mailbox.component';
import { FlashMessagesModule } from 'ngx-flash-messages';
import { RegisterComponent } from './register/register.component';
import { EventComponent } from './admin/event/event.component';
import { AddEventComponent } from './admin/event/add-event/add-event.component';
import { EditEventComponent } from './admin/event/edit-event/edit-event.component';
import { ClubProfileComponent } from './admin/club-profile/club-profile.component';
import { ShowEventComponent } from './admin/event/show-event/show-event.component';
import { SubuserComponent } from './admin/subuser/subuser.component';
import { UserlistComponent } from './admin/userlist/userlist.component';
import { ShowUserComponent } from './admin/userlist/show-user/show-user.component';
import { InviteUserComponent } from './admin/userlist/invite-user/invite-user.component';
import { FeedbackComponent } from './admin/feedback/feedback.component';
import { FollowersComponent } from './admin/followers/followers.component';
import { FollowingComponent } from './admin/following/following.component';
import { PromotersComponent } from './admin/promoters/promoters.component';
import {  EditPramoterComponent } from './admin/promoters/edit-pramoter/edit-pramoter.component';

//services
import { InboxService } from './services/web/inbox.service';
import { AuthService } from './services/web/auth.service';
import {HomeService} from './services/web/home.service';
import {QuestionControlService} from './services/web/question-control.service';
import { GlobalUtility } from './services/web/globalUtility';
import {EventService} from './services/web/event.service';
import { AuthGuard } from './services/web/auth.guard';
import { MediaComponent } from './admin/media/media.component';
import { ChoiceComponent } from './admin/choice/choice.component';
import { AddChoiceComponent } from './admin/choice/add-choice/add-choice.component';
import { EditChoiceComponent } from './admin/choice/edit-choice/edit-choice.component';
import { ShowChoiceComponent } from './admin/choice/show-choice/show-choice.component';
import { CommanService } from './services/comman.service';
import { ProfileService } from './services/web/profile.service';
import { MediaService } from './services/web/media.service';
import { InvitePromoterComponent } from './admin/invite-promoter/invite-promoter.component';
import {NetworkComponent } from './admin/network/network.component';
import { GuestlistComponent } from './admin/guestlist/guestlist.component';
import { ClubguestlistComponent } from './admin/clubguestlist/clubguestlist.component';
import{ PramotorService} from '../app/services/web/pramotor.service';
import { ShowPramoterComponent } from './admin/promoters/show-pramoter/show-pramoter.component';

@NgModule({
  exports: [
    CdkTableModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
  ],
  declarations: []
})

export class DemoMaterialModule {}

@NgModule({
  declarations: [    
    ClubguestlistComponent,
    NetworkComponent,
    GuestlistComponent,
    InvitePromoterComponent,
    ShowEventComponent,
    AddEventComponent,
    EventComponent,
    DynamicFormQuestionComponent,
    DynamicFormComponent,
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    LoginComponent,
    FooterComponent,
    AdminComponent,
    AccountSettingComponent,
    HomeComponent,
    TermsComponent,
    MailboxComponent,
    RegisterComponent,
    EditEventComponent,
    ChoiceComponent,
     AddChoiceComponent,
      EditChoiceComponent, 
      ShowChoiceComponent,
    ClubProfileComponent,
    MediaComponent,
    SubuserComponent,
    UserlistComponent,
    ShowUserComponent,
    InviteUserComponent,
    FeedbackComponent,
    FollowersComponent,
    FollowingComponent,
    PromotersComponent,
    EditPramoterComponent,ShowPramoterComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    DemoMaterialModule,
    FormsModule,
    RouterModule.forRoot(AppRoute),
    //FusionChartsModule,
    FlashMessagesModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyDhbFDTNCgD8q92wVxd7fwCuGjfaahDsN0",
      libraries: ["places"]
    }),
    AgmSnazzyInfoWindowModule
  ],
  exports:[],
  providers: [CommanService,EventService,AuthGuard,InboxService,AuthService,HomeService,QuestionControlService,GlobalUtility,ProfileService, MediaService,PramotorService],
  bootstrap: [AppComponent]
})
export class AppModule { }

//platformBrowserDynamic().bootstrapModule(AppModule);
