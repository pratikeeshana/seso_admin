import { Injectable } from '@angular/core';

import {constants} from '../config/constants';
import {apis} from '../config/apis';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';
import { Http, Headers, RequestOptions } from '@angular/http';


@Injectable()
export class CommanService{
    
    Api_url = ''
    user_info = localStorage.getItem('user_info') ? JSON.parse(localStorage.getItem('user_info')) : '';
    private headers = new Headers({ 'accesstoken': this.user_info.token });
    private options = new RequestOptions({ headers: this.headers });
  
    
    constructor(private http: Http) { 

    }
    
    getData(data,url){ 
        return this.http.get(constants.host +url,this.options).map(data => data.json());
    } 
    postData(data,url){
        return this.http.post(constants.host +url,data,this.options).map(data => data.json());
    }

    deleteData(data){
        return this.http.post(constants.host + apis.DELETE_USER,data,this.options).map(data => data.json());
    }

}