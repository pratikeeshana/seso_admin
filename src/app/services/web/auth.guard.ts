import { Injectable } from '@angular/core';
import { CanActivate,Router } from '@angular/router';



import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';
import { Http, Headers, RequestOptions } from '@angular/http';


//constants
import {routes} from '../../config/routes';
import {constants} from '../../config/constants';
import {apis} from '../../config/apis';

@Injectable()
export class AuthGuard implements CanActivate {

    Api_url = ''
    user_info ;
    headers;
    options;

    constructor(private router: Router,private http: Http) { }

    canActivate() {
        

        // let obj_user =JSON.parse(localStorage.getItem('user_info'));
        // console.log(obj_user);
        // console.log(obj_user[0]['user_type']);

        if ( localStorage.getItem('user_info')) {

            this.user_info = JSON.parse(localStorage.getItem('user_info'));
            this.headers = new Headers({ 'accesstoken': this.user_info.token });
            this.options = new RequestOptions({ headers: this.headers });
            
            this.checklogin().subscribe(
                res =>{
               
                     
                    if(!res['result'] && this.router.url != '/login' )
                    {
                        localStorage.setItem('user_info','');
                        this.router.navigate([routes.login]);
                        
                    }
                    else
                    {
                        return true;
                    }
                  
                },
                err => {
                    return false;
                }
          
              )
            // if(!res['result'] && this.router.url != '/login' )
            // {
            //     localStorage.setItem('user_info','');
            //     this.router.navigate([routes.login]);
            // }
           // console.log(JSON.parse(localStorage.getItem('user_info')));
            return true;
        }
        else{
            this.router.navigate([routes.login]);
            //return false;
        }
      
        
    }

    public checklogin(){ 
     
        
        return this.http.get(constants.host + apis.CHECK_LOGIN,this.options).map(data => data.json());
    }
}
