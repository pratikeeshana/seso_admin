import { Injectable } from '@angular/core';

import { constants } from '../../config/constants';
import { apis } from '../../config/apis';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';
import { Http, Headers, RequestOptions } from '@angular/http';



@Injectable()
export class InboxService {

    Api_url = ''
    user_info = JSON.parse(localStorage.getItem('user_info'));
    private headers = new Headers({ 'accesstoken': this.user_info.token });
    private options = new RequestOptions({ headers: this.headers });


    constructor(private http: Http) {


    }

    public getClubApplication() {
        
       
        return this.http.get(constants.host + apis.GET_CLUB_APP, this.options).map(data => data.json());
    }
    acceptClubApplication(data) {
        
        return this.http.post(constants.host + apis.ACCEPT_CLUB_APP, data,this.options).map(data => data.json());
    }
}