import { Injectable } from '@angular/core';
import { apis } from '../../config/apis';
import { constants } from '../../config/constants';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class MediaService {

  user_info = JSON.parse(localStorage.getItem('user_info'));
  private headers = new Headers({ 'accesstoken': this.user_info.token });
  private options = new RequestOptions({ headers: this.headers });

  constructor(private http: Http) { }

  public addMedia(mediadt) {
    return this.http.post(constants.host + apis.CREATE_MEDIA, mediadt, this.options).map(data => data.json());
  }
  public deleteMedia(mediadt) {
    return this.http.post(constants.host + apis.DELETE_MEDIA, mediadt, this.options).map(data => data.json());
  }

}
