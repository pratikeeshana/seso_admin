import { Injectable } from '@angular/core';

import {constants} from '../../config/constants';
import {apis} from '../../config/apis';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';
import { Http, Headers, RequestOptions } from '@angular/http';

import { Router } from '@angular/router';
import {routes} from '../../config/routes';


@Injectable()
export class AuthService{
    
    Api_url = ''
    private headers = new Headers({ 'Content-Type': 'application/json'});
    private options = new RequestOptions({ headers: this.headers });
    arrRoutes = routes;
    
    constructor(private http: Http,private _router: Router) { 

    }
    
    public login(data){  
        
        return this.http.post(constants.host + apis.USER_LOGIN,JSON.stringify(data),this.options).map(data => data.json());
    }
    register(data){
        debugger;
        return this.http.post(constants.host + apis.USER_CREATE,JSON.stringify(data),this.options).map(data => data.json());
    }

    public logout() {
        localStorage.setItem('user_info','');
        this._router.navigate([this.arrRoutes.login]);
    }
}