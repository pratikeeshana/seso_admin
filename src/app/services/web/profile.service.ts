import { Injectable } from '@angular/core';
import { apis } from '../../config/apis';
import { constants } from '../../config/constants';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class ProfileService {

  user_info = JSON.parse(localStorage.getItem('user_info'));
  private headers = new Headers({ 'accesstoken': this.user_info.token });
  private options = new RequestOptions({ headers: this.headers });

  constructor(private http: Http) { }

  public getProfile() {
  
    let id = "abc";
    return this.http.get(constants.host + apis.GET_CLUB_PROFILE + this.user_info._id + '/'  + id, this.options).map(data => data.json());
  }

 

  editProfile(data) {
    debugger;
    return this.http.post(constants.host + apis.UPDATE_USER,data,this.options).map(data => data.json());
}

}
