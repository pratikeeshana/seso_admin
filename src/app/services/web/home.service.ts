import { Injectable } from '@angular/core';

import {constants} from '../../config/constants';
import {apis} from '../../config/apis';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';
import { Http, Headers, RequestOptions } from '@angular/http';


@Injectable()
export class HomeService{
    
    Api_url = ''
    private headers = new Headers({ 'Content-Type': 'application/json'});
    private options = new RequestOptions({ headers: this.headers });
  
    
    constructor(private http: Http) { 

    }
    
    public getQuestionaire(){ 
     
        
        return this.http.get(constants.host + apis.GET_QUESTION,this.options).map(data => data.json());
    } 

    submitClubApplication(data){
        return this.http.post(constants.host + apis.SUB_QUESTION,data,this.options).map(data => data.json());
    }
}