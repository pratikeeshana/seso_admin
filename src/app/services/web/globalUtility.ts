export class GlobalUtility {

    constructor() {}
    
    public localStorageItem(id: string): string {
        return localStorage.getItem(id);
    }
    public localStorageParseItem(id: string): any {
        return JSON.parse(localStorage.getItem(id));
    }
}