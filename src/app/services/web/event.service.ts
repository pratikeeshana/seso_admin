import { Injectable } from '@angular/core';

import {constants} from '../../config/constants';
import {apis} from '../../config/apis';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';
import { Http, Headers, RequestOptions } from '@angular/http';


@Injectable()
export class EventService{
    
    Api_url = ''
    user_info = JSON.parse(localStorage.getItem('user_info'));
    private headers = new Headers({ 'accesstoken': this.user_info.token });
    private options = new RequestOptions({ headers: this.headers });
  
    
    constructor(private http: Http) { 

    }
    
    getEvents(data){ 
        return this.http.post(constants.host + apis.GET_EVENT,data,this.options).map(data => data.json());
    } 

    getData(data,url){ 
        return this.http.get(constants.host +url,this.options).map(data => data.json());
    }


    addEvent(data){
        let  headers = new Headers([{ 'accesstoken': this.user_info.token },{'Content-Type': 'multipart/form-data'}]);
    let  options = new RequestOptions({ headers: this.headers });
        return this.http.post(constants.host + apis.EVENT_CREATE,data,options).map(data => data.json());
    }

    updateEvent(data) {
       // debugger;
        return this.http.post(constants.host + apis.EVENT_UPDATE,data,this.options).map(data => data.json());
    }
    deleteEvent(data) {
        return this.http.post(constants.host + apis.EVENT_DELETE,data,this.options).map(data => data.json());
    }

}