import { Injectable } from '@angular/core';

import {constants} from '../../config/constants';
import {apis} from '../../config/apis';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';
import { Http, Headers, RequestOptions } from '@angular/http';


@Injectable()
export class PramotorService{

    Api_url = ''
    user_info = localStorage.getItem('user_info') ? JSON.parse(localStorage.getItem('user_info')) : '';
    private headers = new Headers({ 'accesstoken': this.user_info.token });
    private options = new RequestOptions({ headers: this.headers });

    constructor(private http: Http) { 
    }

    addPramotor(dt){
        let  headers = new Headers([{ 'accesstoken': this.user_info.token },{'Content-Type': 'multipart/form-data'}]);
        let  options = new RequestOptions({ headers: this.headers });
        return this.http.post(constants.host + apis.ADD_PRAMOTOR,dt,this.options).map(data => data.json());
    }

    getAllPramotors(){ 
        let userdt={
            "userType": "3"
        };
        return this.http.post(constants.host + apis.GET_ALL_PRAMOTORS,userdt,this.options).map(data => data.json());
    } 

    getPramoters(data){ 
       // console.log(data);    
       return this.http.get(constants.host + apis.USER_GET + data._id + '/' +this.user_info._id ,this.options).map(data => data.json());
    } 

   

    deletePramoter(data) {
        return this.http.post(constants.host + apis.DELETE_USER,data,this.options).map(data => data.json());
    }

    updatePramoter(data) {
         return this.http.post(constants.host + apis.UPDATE_USER,data,this.options).map(data => data.json());
     }

}
