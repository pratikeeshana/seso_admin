import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';

import * as moment from 'moment'; // import moment.

//models
import { Users } from '../models/user'; // model

//services
import { CommanService } from '../services/comman.service';
import { AuthService } from '../services/web/auth.service';
import { GlobalUtility } from '../services/web/globalUtility';

//plugins
import swal from 'sweetalert2';
import { FlashMessagesService } from 'ngx-flash-messages';

//constants
import {routes} from '../config/routes';
import {apis} from '../config/apis';

declare var $;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(public commanService:CommanService, private route: ActivatedRoute,private _router: Router,private authService:AuthService,private flashMessagesService:FlashMessagesService,private utility :GlobalUtility) { }

  model = new Users('','', 0, '','', '', '', '', '','', '', '', '', '', 0, '', '', '', {},'');
  isPassCorrect = false;
  dateValue =this.getCurrentTime();
  loading = false;
  verifyCode;
  userType;
  arrQuestion;
  showbtn:boolean=false;
  isLinear = true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  getCurrentTime(){
    return moment().format('YYYY-MM-DD'); 
 }

  ngOnInit() {
    
    this.commanService.getData({},apis.GET_CHOICES).subscribe(
      res => {
        if (res.result) {
          this.arrQuestion = res.data;
          for (let index = 0; index < this.arrQuestion.length; index++) {
            this.arrQuestion[index]['is_select'] = [];
            
          }
        }
      },
      err => {

      }
    )

    this.route.params.subscribe(params => {
      this.verifyCode = +params['verifyCode'];
     

    
        this.userType = +params['type'];
          // In a real app: dispatch action to load the details here.
        });
     // console.log(this.userType );
  }

  // setForm() {

  // }

  submitForm(model,formvalid) {
    
   
    let arrInterest=[];
    this.arrQuestion.forEach(element => {

      let temp = {};
      temp['choiceQid'] = element._id;
      
      temp['optionIds'] = [];
      element.is_select.forEach(element2 => {
        let optionId = {};
        optionId['optionId'] = element2._id;
        temp['optionIds'].push(optionId)
      });
      arrInterest.push(temp);
    });

   

    this.model['interests'] = arrInterest;
    if(this.userType == '3')
    { 
      this.model['userType'] = '3';
    }

    if (formvalid) {
      //console.log(model);
    
      this.model['dob'] = this.dateValue;
      this.model['verifyCode'] = this.verifyCode;
      this.authService.register(this.model).subscribe(

        data => {


          if (data.result) {
            localStorage.setItem('user_info', JSON.stringify(data.data));
            this._router.navigate([routes.dashboard]);
          }
          else {
            this.flashMessagesService.show(data.message, {
              classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
              timeout: 5000, // Default is 3000
            });
          }


        },
        error => {
          swal(error);
          this.loading = false;
        });

    }
    else {

      this.flashMessagesService.show("Please fill all details", {
        classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
        timeout: 5000, // Default is 3000
      });

    }

    // this._router.navigate(['/dashboard']);
    // window.location.reload();
  }

  checkPassword(event) {
    let passVal = event.target.value;
    if(this.model.password === passVal)
    {
      this.isPassCorrect = true;
    }
    else
      this.isPassCorrect = false;
  }

  addElement(title:string,index,qId) {
    console.log(title);
    let input={};
    input['data'] = { option:title};
    input['condn'] = { _id:qId};
    this.commanService.postData(input,apis.CHOICE_PUSH).subscribe(

      data => {


        if (data.result) {

          let objOption  = {}
          objOption['option'] = title;
          objOption['_id'] = "dfdsfsdf";
          this.arrQuestion[index].arrOption.push(objOption);

        }
        else {
          this.flashMessagesService.show(data.message, {
            classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
            timeout: 5000, // Default is 3000
          });
        }


      },
      error => {
        swal(error);
      
      });
    

    
    //$("<label class='checkbox-inline custom-checkbox' style='margin:5px 10px'>"+title+" <input type='checkbox' value="+title+"> <span class='checkmark'></span>  </label>").insertBefore(".addlbl");
  }

  changeOption(i,option) {
    

   
     

      this.arrQuestion[i]['is_select'].push(option);
      console.log(this.arrQuestion);
  }

}
