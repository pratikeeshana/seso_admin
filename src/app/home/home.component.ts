import { Component, OnInit ,Input} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

//service
import {HomeService} from '../services/web/home.service';
import { QuestionControlService }    from '../services/web/question-control.service';

//plugins
import { FlashMessagesService } from 'ngx-flash-messages';

//models
//import {Questionaire} from '../models/questionaire';

import { QuestionBase } from '../models/question-base';
import { checkboxQuestion } from '../models/question-checkbox';

import { TextboxQuestion }  from '../models/question-textbox';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  isLinear = false;
  arrQuestionaire ;


  @Input() questions: QuestionBase<any>[] = [];
  form: FormGroup;
  payLoad = '';
  constructor(private qcs:QuestionControlService,private _formBuilder: FormBuilder,private homeService:HomeService,private flashMessagesService:FlashMessagesService) { 
   
    
    // let questions: QuestionBase<any>[] = [

    //   new checkboxQuestion({
    //     key: 'brave',
    //     label: 'Bravery Rating',
    //     options: [
    //       {key: 'solid',  value: 'Solid'},
    //       {key: 'great',  value: 'Great'},
    //       {key: 'good',   value: 'Good'},
    //       {key: 'unproven', value: 'Unproven'}
    //     ],
    //     order: 3
    //   }),

    //   new TextboxQuestion({
    //     key: 'firstName',
    //     label: 'First name',
    //     value: 'Bombasto',
    //     required: true,
    //     order: 1
    //   }),

    //   new TextboxQuestion({
    //     key: 'emailAddress',
    //     label: 'Email',
    //     type: 'email',
    //     order: 2
    //   })
    // ];
    // this.arrQuestionaire =  questions.sort((a, b) => a.order - b.order);
  }

  ngOnInit() {

   
    this.firstFormGroup = this._formBuilder.group({
      userName: ['', Validators.required],
      email: ['', Validators.required],
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });


  //   this.firstFormGroup = new FormGroup({  

  //     gender: new FormControl()
  //  }); 

    this.homeService.getQuestionaire().subscribe(
      data => {
        
        
        if(data.result)
        {
          //this.arrQuestionaire = data.data; 


          let questions: QuestionBase<any>[] = [];
          
          data.data.forEach(element => {
            let temp;

            if(element.type=='3')
            {
              let arrOptions = [];
              element.arrOption.forEach(option => {
                let tmpOpVal = {};
                tmpOpVal['key'] = option._id;
                tmpOpVal['value'] = option.option;
                arrOptions.push(tmpOpVal);
              });
              temp = new checkboxQuestion({
                key: element._id,
                label: element.question,
                options: arrOptions,
                order: 3
              }),
              questions.push(temp);
            }
            else if(element.type=='1')
            {
              temp = new TextboxQuestion({
                key: element._id,
                label: element.question,
                options: [],
                order: 3
              }),
              questions.push(temp);
            }
            
          });
          
          

           
      
          this.arrQuestionaire =  questions.sort((a, b) => a.order - b.order);
        }
        else{
          this.flashMessagesService.show(data.message, {
            classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
            timeout: 5000, // Default is 3000
          });
        }
       
        
      },
      error => {
        this.flashMessagesService.show("Something went wrong.", {
          classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
          timeout: 5000, // Default is 3000
        });
      });
  
  }

  submitUserDetail(model, formvalid) {
     
   
    console.log(model);
    console.log(formvalid);
    if(formvalid)
      localStorage.setItem('clubUserInfo',JSON.stringify(model));
    else{
      this.flashMessagesService.show("Fill all fields properly", {
        classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
        timeout: 5000, // Default is 3000
      });
    }




     
  }



}
