import { Component, Input, OnInit }  from '@angular/core';
import { FormGroup }                 from '@angular/forms';
import { QuestionBase } from '../models/question-base';
import { Router } from '@angular/router';

import { QuestionControlService }    from '../services/web/question-control.service';
import { HomeService }    from '../services/web/home.service';
import { FlashMessagesService } from 'ngx-flash-messages';
import swal from 'sweetalert2';
declare var $:any;
@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  providers: [ QuestionControlService ],
  styleUrls: ['./home.component.css']
})

export class DynamicFormComponent implements OnInit {
  
  @Input() questions: QuestionBase<any>[] = [];
  form: FormGroup;
  payLoad = '';
  $;
  constructor(private _router: Router,private qcs: QuestionControlService,private homeService:HomeService,private flashMessagesService:FlashMessagesService) {  }

  ngOnInit() {
    this.form = this.qcs.toFormGroup(this.questions);
  }

  onSubmit() {
    this.payLoad = JSON.stringify(this.form.value);
    
    let clubUserInfo = JSON.parse(localStorage.getItem('clubUserInfo'));

    let input  = {};
    input['name'] = clubUserInfo.userName;
    input['email'] = clubUserInfo.email;
     
    input['queSubmission'] = [];
    let queSubmission = [];
    this.questions.forEach(element => {
      let temp  = {};
    

      temp['key'] = element.label;
      temp['value'] = this.form.value[element.key];
      queSubmission.push(temp);

    });
   
    input['queSubmission'] = queSubmission;
    console.log(input);
    this.homeService.submitClubApplication(input).subscribe(

      data => {


        if (data.result) {
          //localStorage.setItem('user_info', JSON.stringify(data.data));
          swal("Application submitted successfully.");

          $('#formModel').modal('hide');
         
          //this._router.navigate(['/dashboard']);
        }
        else {
          this.flashMessagesService.show(data.message, {
            classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
            timeout: 5000, // Default is 3000
          });
        }


      },
      error => {
        this.flashMessagesService.show(error, {
          classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
          timeout: 5000, // Default is 3000
        });
      });
  }
}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/