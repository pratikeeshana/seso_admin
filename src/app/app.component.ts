import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  checkRoute: boolean = true;
  route: string;

  constructor(location: Location,private router: Router) {
  //  let loggedUser = sessionStorage.getItem('uname');
  //  router.events.subscribe((val) => {
      if(location.path() != ''){    
        this.checkRoute=false;   
      } else {
        this.checkRoute=true;
      }
  //  });
    }

}
