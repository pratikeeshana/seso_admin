import { Component } from '@angular/core';

import { LoginComponent } from './login/login.component';
import { AppComponent } from './app.component'; 
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { AdminComponent } from './admin/admin.component';
import { FooterComponent } from './admin/footer/footer.component';
import { AccountSettingComponent } from './admin/account-setting/account-setting.component';
import { HomeComponent } from './home/home.component';
import { TermsComponent } from './terms/terms.component';
import { MailboxComponent } from './admin/mailbox/mailbox.component';
import { RegisterComponent } from './register/register.component';
import { EventComponent } from './admin/event/event.component';
import { AuthGuard } from './services/web/auth.guard';
import { routes } from './config/routes'
import { ChoiceComponent } from './admin/choice/choice.component';
import { ClubProfileComponent } from './admin/club-profile/club-profile.component';
import { MediaComponent } from './admin/media/media.component';
import { InvitePromoterComponent } from './admin/invite-promoter/invite-promoter.component';
import { SubuserComponent } from './admin/subuser/subuser.component';
import { UserlistComponent } from './admin/userlist/userlist.component';
import { FeedbackComponent } from './admin/feedback/feedback.component';
import { NetworkComponent } from './admin/network/network.component';
import { FollowersComponent } from './admin/followers/followers.component';
import { FollowingComponent } from './admin/following/following.component';
import { PromotersComponent } from './admin/promoters/promoters.component';
import { GuestlistComponent } from './admin/guestlist/guestlist.component';
import { ClubguestlistComponent } from './admin/clubguestlist/clubguestlist.component';

export const AppRoute = [
    { path: '', component: HomeComponent },    
  
    { path: 'home', component: HomeComponent },
    { path: 'terms', component: TermsComponent },
    { path: 'login', component: LoginComponent },
    { path: 'register/:type/:verifyCode', component: RegisterComponent },
    {
        path: 'admin', component: AdminComponent,
        children: [
           { path: '', redirectTo: 'dashboard', pathMatch: 'full',canActivate: [AuthGuard]},
           { path: 'dashboard', component: DashboardComponent ,canActivate: [AuthGuard] },
           { path: 'account-setting', component: AccountSettingComponent,canActivate: [AuthGuard] },
           { path: 'mailbox', component: MailboxComponent,canActivate: [AuthGuard] },
           { path: 'event', component: EventComponent,canActivate: [AuthGuard] },
           { path: 'choice', component: ChoiceComponent,canActivate: [AuthGuard] },
           { path: 'profile-setting', component: ClubProfileComponent,canActivate: [AuthGuard] },
           { path: 'media', component: MediaComponent,canActivate: [AuthGuard] },
           { path: 'invite-promoter', component: InvitePromoterComponent,canActivate: [AuthGuard] },
           { path: 'subuser', component: SubuserComponent,canActivate: [AuthGuard] },
           { path: 'userlist', component: UserlistComponent,canActivate: [AuthGuard] },
           { path: 'feedback', component: FeedbackComponent,canActivate: [AuthGuard] },
           { path: 'guestlist', component: GuestlistComponent,canActivate: [AuthGuard] },
           { path: 'club-guestlist/:eventId', component: ClubguestlistComponent,canActivate: [AuthGuard] },
           { path: 'followers', component: FollowersComponent,canActivate: [AuthGuard] },
           { path: 'followings', component: FollowingComponent,canActivate: [AuthGuard] },
           { path: 'promotors', component: PromotersComponent,canActivate: [AuthGuard] }
        ]
    },

];  
