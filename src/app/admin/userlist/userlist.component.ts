import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { CommanService } from '../../services/comman.service';
import { apis} from '../../config/apis';
import { ShowUserComponent } from './show-user/show-user.component';

export interface Element {
  userName: string;
  email: string;
  userType: string;
  contact: string;
  feedback: string[];
  actions: string;
}
var ELEMENT_DATA: Element[] = [];
declare var $: any;

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

  @ViewChild(ShowUserComponent ) child2: ShowUserComponent; 
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  objUser={};
  displayedColumns = ['userName', 'email', 'userType', 'contact','feedback','actions'];
  dataSource;
  showLoader:boolean=false;
  showError:boolean=false;
//  user_info = JSON.parse(localStorage.getItem('user_info'));
  userdt={
   
  };


  constructor(private commanService: CommanService) { }

  ngOnInit() {
    this.getList(this.userdt);
  }

  getList(data) {
    this.commanService.postData(data,apis.GET_ALL_USERS).subscribe(
      res => {
        if (res.result) {
          this.showLoader=true;
          ELEMENT_DATA = res.data;
          this.dataSource = new MatTableDataSource<Element>(ELEMENT_DATA);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      },
      err => {
        this.showError=true;
      }

    )
  }

 
  showUser(userId) {
    let data={};
    let api=apis.GET_CLUB_PROFILE+""+userId;
    this.commanService.getData(data,api).subscribe(
      res => {
        if (res.result) {
          this.objUser = res.data;
          $('#showUser').modal('show');
        }
      },
      err => {
        this.showError=true;
      }
    )
  }

}
