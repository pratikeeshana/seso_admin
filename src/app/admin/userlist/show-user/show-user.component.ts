import { Component, OnInit, Input } from '@angular/core';
import { CommanService } from '../../../services/comman.service';
import {apis} from '../../../config/apis';

@Component({
  selector: 'app-show-user',
  templateUrl: './show-user.component.html',
  styleUrls: ['./show-user.component.css']
})
export class ShowUserComponent implements OnInit {

  @Input() objUser;
  userDetails = {}; 

  constructor(private commanService: CommanService) { }

  ngOnInit() {
    
  }

}
