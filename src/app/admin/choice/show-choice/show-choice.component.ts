import { Component, OnInit ,EventEmitter,Output,Input } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';

//constants
import {routes} from '../../../config/routes';


@Component({
  selector: 'app-show-choice',
  templateUrl: './show-choice.component.html',
  styleUrls: ['./show-choice.component.css']
})
export class ShowChoiceComponent implements OnInit {

  @Input() objChoice;


  options;
  locChoice = {}; 
  constructor() { }

  ngOnInit() {
  }



  getShowChoice() {
    
    //this.model = new Events(this.objEvent._id,this.objEvent.eventName,this.objEvent.description,this.objEvent.eventDate,this.objEvent.minAgeLimit,this.objEvent.maxAgeLimit,this.objEvent.location,this.objEvent.services);
    
    this.locChoice =  this.objChoice
   
  }
}
