import { Component, Output, OnInit, ViewChild, EventEmitter } from '@angular/core';

import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { GlobalUtility } from '../../services/web/globalUtility';

import { EditChoiceComponent } from './edit-choice/edit-choice.component'

//
import { CommanService } from '../../services/comman.service';
import {apis} from '../../config/apis';
//plugins
import swal from 'sweetalert2';

export interface Element {
  title: string;
  question: string;
  date: string;
  actions: string;
}
var ELEMENT_DATA: Element[] = [];
declare var $: any;

@Component({
  selector: 'app-choice',
  templateUrl: './choice.component.html',
  styleUrls: ['./choice.component.css']
})
export class ChoiceComponent implements OnInit {

  @ViewChild(EditChoiceComponent ) child: EditChoiceComponent; 
  // @ViewChild(ShowEventComponent ) child2: ShowEventComponent; 

  @Output()
  ParentEvent = new EventEmitter<string>();

  displayedColumns = ['title', 'question', 'date', 'actions'];
  dataSource;

  objChoice={};


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private commanService: CommanService, public utility: GlobalUtility) { }
  ngAfterViewInit() {

    this.getChoices({});

  }



  ngOnInit() {
  }
  getChoices(data={}) {
    this.commanService.getData(data,apis.GET_CHOICES).subscribe(
      res => {
        if (res.result) {
          ELEMENT_DATA = res.data;
          this.dataSource = new MatTableDataSource<Element>(ELEMENT_DATA);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      },
      err => {

      }

    )
  }

  editChoice(choiceId) {
    
    let input = {};
    input['_id'] = choiceId;
    
    this.commanService.getData({},apis.GET_CHOICES+'/'+choiceId).subscribe(
      res => {
        if (res.result) {
          this.objChoice = res.data[0];        
          
          setTimeout(() => {
            $('#edit-choice').modal('show');
            this.child.getEditChoice();
          }, 1000);
          
        }
      },
      err => {

      }
    )
  }

  showChoice(choiceId) {
    let input = {};
    //input['_id'] = choiceId;
    // debugger;
    this.commanService.getData(input,apis.GET_CHOICES+'/'+choiceId).subscribe(
      res => {
        if (res.result) {
          this.objChoice = JSON.parse(JSON.stringify(res.data[0]));
         
          $('#show-choice').modal('show');
         
          
        }
      },
      err => {

      }
    )
  }
  getChoice() {
    this.ParentEvent.emit('this is a test');
  }

  deleteChoice(choiceId) {


    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Choice Question!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {

        // let input = {};
        // input['_id'] = choiceId;
        //debugger;
        this.commanService.deleteData(apis.DELETE_CHOICE+'/'+choiceId).subscribe(
          res => {
            if (res.result) {
              
             this.getChoices({});
             swal(
              'Deleted!',
              'Your Choice has been deleted.',
              'success'
            )
            }
          },
          err => {
            swal(
              'Error',
              'Something went wrong :(',
              'error'
            )
          }
        )
       
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal(
          'Cancelled',
          'Your event is safe :)',
          'error'
        )
      }
    })

   
  }
}



