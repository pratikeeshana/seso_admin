import { Component, OnInit, EventEmitter, Output,ChangeDetectorRef ,Input} from '@angular/core';
import { MatChipInputEvent } from '@angular/material';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

//services
import { CommanService } from '../../../services/comman.service'
import { GlobalUtility } from '../../../services/web/globalUtility';

//models
import { Choices } from '../../../models/choice';

//constants
import { routes } from '../../../config/routes';
import {apis} from '../../../config/apis';

//plugins
import swal from 'sweetalert2';
import { FlashMessagesService } from 'ngx-flash-messages';


declare var $: any;

@Component({
  selector: 'app-edit-choice',
  templateUrl: './edit-choice.component.html',
  styleUrls: ['./edit-choice.component.css']
})
export class EditChoiceComponent {

 
  @Output()
  childEvent = new EventEmitter<string>();
  

  @Input() objChoice;

  model = new Choices('', '', '', '','');
  
  constructor(private route: ActivatedRoute,private _router: Router,private commanService:CommanService,private flashMessagesService:FlashMessagesService,private utility :GlobalUtility) { }

  loading:boolean = false;
  visible: boolean = true;
  selectable: boolean = true;
  removable: boolean = true;
  addOnBlur: boolean = true;

  // Enter, comma
  separatorKeysCodes = [ENTER, COMMA];

  options;

  submitForm(model, formvalid) {
    debugger;
    if (formvalid == true) {
      let input ={};
      model['arrOption'] = this.options;
      input['data'] = model;
      input['condn'] = {_id:model._id};
      //delete model._id;
      
      this.commanService.postData(input,apis.USER_UPDATE).subscribe(

        data => {


          if (data.result) {

            $('#edit-choice').modal('hide');
            swal('Choice updated successfully');
            this.getChoices();
          }
          else {
            this.flashMessagesService.show(data.message, {
              classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
              timeout: 5000, // Default is 3000
            });
          }
        },
        error => {
          swal(error);
          this.loading = false;
        });

    }
    else {

      this.flashMessagesService.show("Please all details", {
        classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
        timeout: 5000, // Default is 3000
      });

    }
  }


  add(event: MatChipInputEvent): void {
    let input = event.input;
    let value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.options.push({ option: value.trim() });
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(service: any): void {
    let index = this.options.indexOf(service);

    if (index >= 0) {
      this.options.splice(index, 1);
    }
  }
  getChoices(){
    this.childEvent.emit('this is a test');
    }

  getEditChoice() {
    
    this.model = new Choices(this.objChoice._id,this.objChoice.title,this.objChoice.question,this.objChoice.arrOption,this.objChoice.date);
    console.log(this.model.arrOption);
    this.options = this.model.arrOption;
  }

}
