import { Component, OnInit, Input, ViewChild, EventEmitter, Output } from '@angular/core';
import {PromotersComponent} from '../promoters.component';
import { PramotorService } from '../../../services/web/pramotor.service';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import{ FormGroup, FormControl, Validators } from '@angular/forms';

//models
import {EditPramoters} from '../../../models/pramoters';



//plugins
import swal from 'sweetalert2';
import { FlashMessagesService } from 'ngx-flash-messages';


var ELEMENT_DATA: Element[] = [];
declare var $: any;

@Component({
  selector: 'app-edit-pramoter',
  templateUrl: './edit-pramoter.component.html',
  styleUrls: ['./edit-pramoter.component.css']
})
export class EditPramoterComponent implements OnInit {

  pramoters_data;
  dataSource;
  loading:boolean = false;
  userForm :FormGroup;

  model = new EditPramoters(0,'','', 0, 0, '    ');
  @Input() objEvent;

  @Output()
  childEvent = new EventEmitter<string>();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(  private objPramotorservice: PramotorService,
                private flashMessagesService:FlashMessagesService) { }

  ngOnInit() {
   
    this.getpramotors()
  }

  getChoices(){
    debugger;
    this.childEvent.emit('this is a test');
    }

//   getEditPramoter() {
//     this.pramoters_data = JSON.parse(localStorage.getItem('pramoters_data'));
//     console.log(this.pramoters_data);
//     this.model._id = this.pramoters_data._id;
//     this.model.fullName = this.pramoters_data.fullName;
//     this.model.userName = this.pramoters_data.userName;
//     this.model.userType = this.pramoters_data.userType;
//     this.model.contact = this.pramoters_data.contact;
//     this.model.email = this.pramoters_data.email;

// }

getEditPramoter() {
  this.model = new EditPramoters(
      this.objEvent._id,
      this.objEvent.fullName,
      this.objEvent.userName,
      this.objEvent.userType,
      this.objEvent.contact,
      this.objEvent.email    
  );
}




updateUser(model, formvalid) {
  debugger;
  if (formvalid == true) {
    let input = {      
      'data':model,
      'condn':{'_id':model._id}        
    };
    this.objPramotorservice.updatePramoter(input).subscribe(
      data => {
        if (data.result) 
        {
          this.getChoices();
            $('#edit-pramoter').modal('hide');
            swal('Pramoter updated successfully');
           
        }
        else {
          this.flashMessagesService.show(data.message, {
            classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
            timeout: 5000, // Default is 3000
          });
        }
      },
      error => {
        swal(error);
        this.loading = false;
      });

  }
  else {

    this.flashMessagesService.show("Please Fill all details", {
      classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
      timeout: 5000, // Default is 3000
    });

  }
}


getpramotors(): void{
  this.objPramotorservice.getAllPramotors().subscribe(
    res => {
      if (res.result) {
        ELEMENT_DATA = res.data;
        this.dataSource = new MatTableDataSource<Element>(ELEMENT_DATA);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        let eventData = res.data;
      }
    },
    err => {

    }
  )
}

}
