import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPramoterComponent } from './edit-pramoter.component';

describe('EditPramoterComponent', () => {
  let component: EditPramoterComponent;
  let fixture: ComponentFixture<EditPramoterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPramoterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPramoterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
