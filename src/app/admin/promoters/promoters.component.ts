import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import {routes} from '../../config/routes';
import { PramotorService } from '../../services/web/pramotor.service';
import{ FormGroup, FormControl, Validators } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import {apis} from '../../config/apis';
import { EditPramoterComponent }  from './edit-pramoter/edit-pramoter.component';
import { ShowPramoterComponent }  from './show-pramoter/show-pramoter.component';
import {} from './add-promoter/add-promoter.component';

//plugins
import swal from 'sweetalert2';
import { debounce } from 'rxjs/operator/debounce';
import { FlashMessagesService } from 'ngx-flash-messages';
import { from } from 'rxjs/observable/from';


var ELEMENT_DATA: Element[] = [];
declare var $: any;

export interface Element {
  fullName: string;
  userName: string;
  userType: 3;
  contact: number;
  email: string;
  actions: string;
}

@Component({
  selector: 'app-promoters',
  templateUrl: './promoters.component.html',
  styleUrls: ['./promoters.component.css'],
  providers: [PramotorService]
})
export class PromotersComponent implements OnInit {

  displayedColumns = ['fullName', 'userName','userType', 'contact', 'email', 'actions'];
  dataSource;
  showDetails:boolean=false;
  arrRoutes = routes;
  pramotorsData: any;
  userForm :FormGroup;
  loading: boolean = false;
  objEvent={};


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(EditPramoterComponent ) child: EditPramoterComponent; 
  @ViewChild(ShowPramoterComponent ) child1: ShowPramoterComponent; 

  @Output()
  ParentEvent = new EventEmitter<string>();

  constructor( private objPramotorservice: PramotorService, private flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
    this.userForm = new FormGroup({
      fullName : new FormControl('',[ Validators.required , Validators.pattern('[a-zA-Z][a-zA-Z]+')]),
      userName:new FormControl('', Validators.required),
      userType: new FormControl('', Validators.required  ),
      password: new FormControl('', [ Validators.required, Validators.minLength(4) ]),
      contact: new FormControl('', [ Validators.required, Validators.pattern('[0-9]*'), Validators.maxLength(10),  Validators.minLength(10) ]),
      email: new FormControl('', [ Validators.required, Validators.pattern("[^ @]*@[^ @]*") ]),
    });
  }

  ngAfterViewInit() {
     this.getpramotors();
   }

  pramotorsModel ={
    fullName: '',
    userName: '',
    userType: '',
    password: '',
    contact: '',
    email: '',

  }

  showDetail()
  {
    this.showDetails=true;
  }

  hideDetails()
  {
    this.showDetails=true;
  }

  addPramotor(){
    debugger;
    this.objPramotorservice.addPramotor(this.pramotorsModel).subscribe(
      objResponse => {
        if(objResponse.result){
          this.getpramotors();
          $('#myModal').modal('hide');         
          swal('Pramotor added successfully');     
        }
        else {
          this.flashMessagesService.show(objResponse.message, {
            classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
            timeout: 5000, // Default is 3000
          });
         
        }
      },
      error => {
        swal(error);
        this.loading = false;
      });
 }

  getChoice() {
    this.ParentEvent.emit('this is a test');
  }

  getpramotors(): void{
    debugger;
    this.objPramotorservice.getAllPramotors().subscribe(
      res => {
        if (res.result) {
          ELEMENT_DATA = res.data;
         //  console.log('res.data', res.data.userType);
          this.dataSource = new MatTableDataSource<Element>(ELEMENT_DATA);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          let eventData = res.data;
         // console.log('res.data', eventData);
        }
      },
      err => {

      }

    )
  }


  editPramoter(pId) {
    let input = {};
    input['_id'] = pId ;
    this.objPramotorservice.getPramoters(input).subscribe(
      res => {
        if (res.result) {

          this.objEvent = JSON.parse(JSON.stringify(res.data));
         // console.log(this.objEvent);
         //  this.objEvent = res.data;
      // localStorage.setItem('pramoters_data', JSON.stringify(this.objEvent));
          
          setTimeout(() => {
            $('#edit-pramoter').modal('show');
            this.child.getEditPramoter();
          }, 1000);
          
        }
      },
      err => {

      }
    )
  }



  deletePramoter(pId) {
    debugger;
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Event!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {

        let input = {};
        input['userId'] = pId;
        this.objPramotorservice.deletePramoter(input).subscribe(
          res => {
            if (res.result) {
             this.getpramotors();
             swal(
              'Deleted!',
              'Your event has been deleted.',
              'success'
            )
            }
          },
          err => {
            swal(
              'Error',
              'Something went wrong :(',
              'error'
            )
          }
        )
       
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal(
          'Cancelled',
          'Your event is safe :)',
          'error'
        )
      }
    })   
  }

  showPramoters(pId) {
    let input = {};
    input['_id'] = pId;
    this.objPramotorservice.getPramoters(input).subscribe(
      res => {
        if (res.result) {
          this.objEvent = JSON.parse(JSON.stringify(res.data));
          $('#show-pramoter').modal('show');
        }
      },
      err => {
      }
    )
}
 
}


