import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowPramoterComponent } from './show-pramoter.component';

describe('ShowPramoterComponent', () => {
  let component: ShowPramoterComponent;
  let fixture: ComponentFixture<ShowPramoterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowPramoterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowPramoterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
