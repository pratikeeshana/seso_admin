import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder} from '@angular/forms';
import { MediaService} from '../../services/web/media.service';
import { ProfileService } from '../../services/web/profile.service';
import { FlashMessagesService } from 'ngx-flash-messages';
import swal from 'sweetalert2';
import { debuglog } from 'util';
declare var $;

@Component({
  selector: 'app-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.css']
})
export class MediaComponent implements OnInit {
  
  addMediaForm:FormGroup;
  filetoupload:FormControl;
  fileType:FormControl;
  userId:FormControl;
  mediatp:boolean=true;
  //user_info:any={};
  dataSource;
  mediaContent:any;
  loaderFlag:boolean = false;
  @ViewChild('fileInput') fileInput: ElementRef;
  
  user_info = JSON.parse(localStorage.getItem('user_info'));

  constructor(private mediaServ:MediaService,private fb:FormBuilder,private profileServ:ProfileService, private flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
    this.createForm();
    this.getClubProfile();
    $('#mediaEdit').on('hidden.bs.modal', function () {
      $("#mediaPreview").html('');
    });
    //console.log(this.user_info);
  }

  getClubProfile()
  {
   // console.log("get club");
    this.profileServ.getProfile().subscribe(
      data => {
        if (data.result==true) {
          this.loaderFlag = true;
          this.user_info=data.data;     
        // console.log(this.user_info);
          this.addMediaForm.get('userId').setValue(this.user_info._id);
        }
      });
  }

  addMedia(mtype)
  {
   this.addMediaForm.get('fileType').setValue(mtype);
    $("#mediaEdit").modal('show');
    if(mtype==1)
    {
      this.mediatp=true;
    }else if(mtype==2){
      this.mediatp=false;
    }
  }

  createForm() {
    this.addMediaForm = this.fb.group({
      userId: [''],
      fileType: [''],
      filetoupload: ['']
    });
  }

  onFileChange(event) {
    if(event.target.files.length > 0) {
      let file = event.target.files[0];
      this.addMediaForm.get('filetoupload').setValue(file);
     
      if(this.addMediaForm.get('fileType').value==1)
      {
        $("#mediaPreview").html("<img height='100px' width='100px' src='"+URL.createObjectURL(event.target.files[0]) + "'>");
      }else if(this.addMediaForm.get('fileType').value==2){
        $("#mediaPreview").html("<video height='100px' width='100px'><source controls='false' src='"+URL.createObjectURL(event.target.files[0]) + "'></video>");
      }
    }
  }

  private prepareSave(): any {
    let input = new FormData();
    input.append('userId', this.addMediaForm.get('userId').value);
    input.append('fileType', this.addMediaForm.get('fileType').value);
    input.append('uploadedImages', this.addMediaForm.get('filetoupload').value);
    return input;
  }

  onSubmit()
  {
    const formModel = this.prepareSave(); 
    this.mediaServ.addMedia(formModel).subscribe(
    data => {
      if (data.result==true) {
        this.getClubProfile();
        $("#mediaEdit").modal('hide');
        this.flashMessagesService.show("Media added successfully", {
          classes: ['alert', 'alert-success'], // You can pass as many classes as you need
          timeout: 10000, // Default is 3000
        });
      }
    });
  }

  /*--------delete media-------*/
  deleteMedia(mid)
  {
    let mediaid={
      "mediaId" : mid
    };

    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this file!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {   
      this.mediaServ.deleteMedia(mediaid).subscribe(
        data => {
          if (data.result==true) {
            this.getClubProfile();
            swal(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
          }
          else{
            swal(
              'Error',
              'Something went wrong :(',
              'error'
            )
          }
        });
      }
      else if (result.dismiss === swal.DismissReason.cancel) {
        swal(
          'Cancelled',
          'Your file is safe :)',
          'error'
        )
      }
      }),
    err => {
      swal(
        'Error',
        'Something went wrong :(',
        'error'
      )
    }
  }
// View Media 

viewMedia(mediaUrl,mtype)
{
  if(mtype==1)
  {
    this.mediaContent="<img width='100%' src='"+mediaUrl+"'>";
  } else 
  if(mtype==2)
  {
    this.mediaContent="<video height='350' width='100%' controls><source src='"+mediaUrl+"'></video>";
  }
  $("#modal-image").modal('show');
}

}
