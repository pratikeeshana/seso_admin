import { Component, OnInit } from '@angular/core';
import { CommanService }  from '../../services/comman.service';
import {apis} from '../../config/apis';
@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {

  feedbacks;
  questions;
  feedbackMessage;
  constructor(private commanService:CommanService) { }

  ngOnInit() {

    
    this.commanService.getData({},apis.FEEDBACK).subscribe(
      data => {
        if (data.result==true) {
          this.feedbacks=data.data.feedback;     
          this.questions=data.data.question;     
          this.feedbackMessage=data.data.feedbackMessage;     
          // this.addMediaForm.get('userId').setValue(this.user_info._id);
        }
      });
  }
  // isNumber(val) { 
    
  //   console.log(typeof parseInt(val));
  //   return typeof parseInt(val) === 'number'; 
  // }

  createRange(number){
    var items: number[] = [];
    for(var i = 1; i <= number; i++){
       items.push(i);
    }
    return items;
  }

}
