import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
declare var $ :any;
declare const layout:any;
import { CommanService } from '../../services/comman.service';
//services
import { GlobalUtility } from '../../services/web/globalUtility';
import { apis} from '../../config/apis';
import { AgmCoreModule } from '@agm/core';

@Component({
  selector: 'app-root',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  arrData;
  constructor(private commanService:CommanService,private location: Location,public utility :GlobalUtility) { }
  
  loadpage()
  {
    location.reload();
  }

  ngOnInit() {
    //this.loadpage();    

    this.commanService.getData({},apis.DASHBOARD).subscribe(
      res => {
        if (res.result) {
          this.arrData = res.data;
         // console.log(this.arrData);
        }
      },
      err => {
       
      }

    )
    
  }


  // setthemecolor(colorid)
  // {
  //   $("body").removeClass('color-purple').removeClass('color-default').removeClass('color-primary').removeClass('color-red').removeClass('color-green').removeClass('color-orange').removeClass('color-blue');
  //   if(colorid==1)
  //   {
  //     $("body").addClass('color-default');
  //     $(".burger").css('background-color','#181a1d');
  //     $(".fixed-nav").css('background-color','#181a1d');
  //   }if(colorid==2)
  //   {
  //     $("body").addClass('color-primary');
  //     $(".burger").css('background-color','#319DB5');
  //     $(".fixed-nav").css('background-color','#319DB5');
  //   }if(colorid==3)
  //   {
  //     $("body").addClass('color-red');
  //     $(".burger").css('background-color','#c9625f');
  //     $(".fixed-nav").css('background-color','#c9625f');
  //   }if(colorid==4)
  //   {
  //     $("body").addClass('color-green');
  //     $(".burger").css('background-color','#18a689');
  //     $(".fixed-nav").css('background-color','#18a689');
  //   }if(colorid==5)
  //   {
  //     $("body").addClass('color-orange');
  //     $(".burger").css('background-color','#b66d39');
  //     $(".fixed-nav").css('background-color','#b66d39');
  //   }if(colorid==6)
  //   {
  //     $("body").addClass('color-purple');
  //     $(".burger").css('background-color','#7a26bf');
  //     $(".fixed-nav").css('background-color','#7a26bf');
  //   }if(colorid==7)
  //   {
  //     $("body").addClass('color-blue');
  //     $(".burger").css('background-color','#4a89dc');
  //     $(".fixed-nav").css('background-color','#4a89dc');
  //   }
  // }

 
  
}
