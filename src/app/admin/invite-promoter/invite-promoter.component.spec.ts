import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvitePromoterComponent } from './invite-promoter.component';

describe('InvitePromoterComponent', () => {
  let component: InvitePromoterComponent;
  let fixture: ComponentFixture<InvitePromoterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvitePromoterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvitePromoterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
