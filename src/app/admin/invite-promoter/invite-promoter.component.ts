import { Component, OnInit } from '@angular/core';
import { MatChipInputEvent } from '@angular/material';

import { FormControl,FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { map } from 'rxjs/operators/map';

@Component({
  selector: 'app-invite-promoter',
  templateUrl: './invite-promoter.component.html',
  styleUrls: ['./invite-promoter.component.css']
})
export class InvitePromoterComponent implements OnInit {

  promotorForm:FormGroup;
  email:FormControl;
	message:FormControl;

  

  constructor() { }

  ngOnInit() {
    this.createForm();
  }

  onSubmit() {
    
  }

  createForm()
  {
    this.promotorForm = new FormGroup({
   
    email : new FormControl('',[Validators.required, Validators.pattern("[^ @]*@[^ @]*") ]),
    message : new FormControl('',Validators.required),
   
    });
  }

}
