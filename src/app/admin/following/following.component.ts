import { Component, OnInit } from '@angular/core';
import {routes} from '../../config/routes';
import { CommanService }  from '../../services/comman.service';
import {apis} from '../../config/apis';



//plugins
import swal from 'sweetalert2';

@Component({
  selector: 'app-following',
  templateUrl: './following.component.html',
  styleUrls: ['./following.component.css']
})
export class FollowingComponent implements OnInit {
  arrUser ;
  loading:boolean = false;
  arrRoutes = routes;
  objUser = JSON.parse(localStorage.getItem('user_info'));
  constructor(private commanService:CommanService) { }

  ngOnInit() {
    
    this.commanService.postData({userId:this.objUser._id,type:"2"},apis.USER_FOLLOWERS).subscribe(
      data => {
        if (data.result==true) {
          this.arrUser=data.data;     
          if(this.arrUser == 0)
         {
           swal('No Followings For This Club'); 
           this.loading= false; 
         }
        }
      });
  }


  deleteFollower(userId) {


    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Event!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {

        this.commanService.deleteData(apis.USER_FOLLOW+"/"+userId).subscribe(
          data => {
            if (data.result==true) {
              swal(
                'Deleted!',
                'Your event has been deleted.',
                'success'
              )
              this.ngOnInit();
            }
          });
       
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal(
          'Cancelled',
          'Your event is safe :)',
          'error'
        )
      }
    })
  
  }



}
