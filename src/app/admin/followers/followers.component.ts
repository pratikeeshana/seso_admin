import { Component, OnInit } from '@angular/core';
import {routes} from '../../config/routes';
import { CommanService }  from '../../services/comman.service';
import {apis} from '../../config/apis';



//plugins
import swal from 'sweetalert2';
@Component({
  selector: 'app-followers',
  templateUrl: './followers.component.html',
  styleUrls: ['./followers.component.css']
})
export class FollowersComponent implements OnInit {

  loading:boolean = true;
  arrUser ;
  arrRoutes = routes;
  objUser = JSON.parse(localStorage.getItem('user_info'));
  constructor(private commanService:CommanService) { }

  ngOnInit() {
    
    this.commanService.postData({userId:this.objUser._id,type:"1"},apis.USER_FOLLOWERS).subscribe(
      data => {
        if (data.result==true) {
          this.arrUser=data.data;     
         console.log(this.arrUser);
         if(this.arrUser == 0)
         {
           swal('No Followers For This Club'); 
           this.loading= false; 
         }
        }
      });
  }


  deleteFollower(userId) {


    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Event!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {

        this.commanService.deleteData(apis.USER_FOLLOW+"/"+userId).subscribe(
          data => {
            if (data.result==true) {
              swal(
                'Deleted!',
                'Your event has been deleted.',
                'success'
              )
              this.ngOnInit();
            }
          });
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal(
          'Cancelled',
          'Your event is safe :)',
          'error'
        )
      }
    })
  
  }



}
