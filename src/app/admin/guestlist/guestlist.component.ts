import { Component, OnInit } from '@angular/core';
import { routes } from '../../config/routes';
import { CommanService } from '../../services/comman.service';
import { apis } from '../../config/apis';
import * as _ from 'underscore';
import { Router, ActivatedRoute } from '@angular/router';

//plugins
import swal from 'sweetalert2';

@Component({
  selector: 'app-guestlist',
  templateUrl: './guestlist.component.html',
  styleUrls: ['./guestlist.component.css']
})
export class GuestlistComponent implements OnInit {

  arrRoutes = routes;
  objUser = JSON.parse(localStorage.getItem('user_info'));
  arrUser;
  selectedUser;
  constructor(private commanService: CommanService, private route: ActivatedRoute, private _router: Router) { }
  eventId;
  ngOnInit() {
    if (this.objUser.userType == '2') {

    }
    else {
      this.commanService.postData({ userId: this.objUser._id, type: "1" }, apis.USER_FOLLOWERS).subscribe(data => {
        if (data.result == true) {
          this.arrUser = data.data;

        }
      });
    }

      // this.route.params.subscribe(params => {

      //   console.log(params['eventId']);

      //   this.eventId = params['eventId'];
      //   // In a real app: dispatch action to load the details here.
      // });
      // console.log(this.eventId);

  }
  selectUsers() {

    this.selectedUser = _.filter(this.arrUser, function (element) { return element['isSelect'] == true; })

      ;

  }
  selectAll(e) {
    if (e.target.checked) {
      for (let i = 0; i < this.arrUser.length; i++) {
        this.arrUser[i]['isSelect'] = true;
      }
    }
    else {
      for (let i = 0; i < this.arrUser.length; i++) {
        this.arrUser[i]['isSelect'] = false;
      }
    }
  }

  submitGuest() {

    let input = [];
    this.selectedUser.forEach(element => {
      let temp = {};
      temp['userId'] = element.followerUser._id;
      temp['eventId'] = this.eventId;
      input.push(temp);
    });
    this.commanService.postData(input, apis.ADD_GUEST).subscribe(
      data => {
        if (data.result == true) {
          //this.arrUser=data.data;     
          swal(
            'Submited!',
            'Your guest list sent to Club successfully.',
            'success'
          )
        }
      });
  }
}
