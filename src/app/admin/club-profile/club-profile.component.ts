import { Component, OnInit } from '@angular/core';
import { MatChipInputEvent} from '@angular/material';
import { ENTER, COMMA} from '@angular/cdk/keycodes';
import { ProfileService } from '../../services/web/profile.service';
import { FormControl, FormGroup, FormBuilder, Validator, AbstractControl , Validators} from '@angular/forms';


//plugins
declare var $: any;
import swal from 'sweetalert2';
import { FlashMessagesService } from 'ngx-flash-messages';

@Component({
  selector: 'app-club-profile',
  templateUrl: './club-profile.component.html',
  styleUrls: ['./club-profile.component.css']
})
export class ClubProfileComponent implements OnInit {

  showPanel:boolean=false;
  profileData:any={};
  socialUrl:any={};
  loaderFlag:boolean = false;

  editProfileForm:FormGroup;

  constructor(private profileServ:ProfileService,
               private _fb:FormBuilder,
               private flashMessagesService:FlashMessagesService) { }

  ngOnInit() {
    this.getClubProfile();  
    this.createForm();
    // console.log(this.profileData);
  }

  createForm()
  {
    this.editProfileForm = new FormGroup({
      _id:new FormControl(this.profileServ.user_info._id),
      email:new FormControl('',[ Validators.required ,Validators.pattern("[^ @]*@[^ @]*") ]),//
      userName:new FormControl('', Validators.required),//
      relStatus:new FormControl(''),
      state:new FormControl('', Validators.required),//
      workInfo:new FormControl(''),
      schoolInfo:new FormControl(''),
      contact:new FormControl('',[ Validators.required, Validators.pattern("^0|[0-9]\d*$"), Validators.maxLength(10),  Validators.minLength(10) ]),//
      city:new FormControl('', Validators.required),//
      zipCode:new FormControl('',[ Validators.required,Validators.pattern("^0|[0-9]\d*$")]),//
      aboutYou:new FormControl(''),
      socialLinks: this._fb.group({
        twUrl:new FormControl(''),
        fbUrl:new FormControl(''),
        snapUrl:new FormControl(''),
        instaUrl:new FormControl('')    
      })
  });
  }

  getClubProfile()
  {
    this.profileServ.getProfile().subscribe(
      data => {
        if (data.result==true) {
           this.profileData=data.data;     
            this.loaderFlag = true;
           this.socialUrl=data.data.socialLinks;
        }
      });
  }



 editClubUser(profileData,formvalid) {
  debugger;
  // if (formvalid == true) {
    let input = {      
      'data':profileData,
      'condn':{'_id':profileData._id}        
    };
    this.profileServ.editProfile(input).subscribe(
      data => {
        if (data.result) 
        {
          this.showPanel=false;
            swal('User updated successfully');
            this.getClubProfile();
        }
        else {
          this.flashMessagesService.show(data.message, {
            classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
            timeout: 5000, // Default is 3000
          });
        }
      },
      error => {
        swal(error);
        this.loaderFlag = false;
      });

//  }
//   else {

//     this.flashMessagesService.show("Please Fill all details", {
//       classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
//       timeout: 5000, // Default is 3000
//     });

//  }
}


  // editClubUser()
  // {
  //   debugger;
  //   this.profileServ.editProfile(this.editProfileForm.value).subscribe(
  //   data => {
  //     if (data.result==true) {
  //       this.cancelEdit();
  //     }
  //   });
  // }

  visible: boolean = true;
  selectable: boolean = true;
  removable: boolean = true;
  addOnBlur: boolean = true;

  separatorKeysCodes = [ENTER, COMMA];

  fruits = [
    { name: 'Jazz' },
    { name: 'Bluz' },
    { name: 'R&B' },
  ];

  add(event: MatChipInputEvent): void {
    let input = event.input;
    let value = event.value;
    if ((value || '').trim()) {
      this.fruits.push({ name: value.trim() });
    }
    if (input) {
      input.value = '';
    }
  }

  remove(fruit: any): void {
    let index = this.fruits.indexOf(fruit);

    if (index >= 0) {
      this.fruits.splice(index, 1);
    }
  }

  editProfile()
  {
  this.showPanel=true;
  }
  cancelEdit()
  {
    this.getClubProfile();  
    this.showPanel=false;

  }
}
