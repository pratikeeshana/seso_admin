import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClubguestlistComponent } from './clubguestlist.component';

describe('ClubguestlistComponent', () => {
  let component: ClubguestlistComponent;
  let fixture: ComponentFixture<ClubguestlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClubguestlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClubguestlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
