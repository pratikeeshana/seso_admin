import { Component, OnInit } from '@angular/core';
import { routes } from '../../config/routes';
import { CommanService } from '../../services/comman.service';
import { apis } from '../../config/apis';
import * as _ from 'underscore';
import { Router, ActivatedRoute } from '@angular/router';

//plugins
import swal from 'sweetalert2';

@Component({
  selector: 'app-clubguestlist',
  templateUrl: './clubguestlist.component.html',
  styleUrls: ['./clubguestlist.component.css']
})
export class ClubguestlistComponent implements OnInit {

  arrRoutes = routes;
  objUser = JSON.parse(localStorage.getItem('user_info'));
  arrUser;
  selectedUser;
  constructor(
    private router: Router,private commanService: CommanService, private route: ActivatedRoute, private _router: Router) { }
  eventId;
  ngOnInit() {

    this.route.params.subscribe(params => {

      console.log(params['eventId']);

      this.eventId = params['eventId'];
      // In a real app: dispatch action to load the details here.
    });
    this.commanService.getData({}, apis.ADD_GUEST+"/"+this.eventId).subscribe(data => {
      if (data.result == true) {
        this.arrUser = data.data;

      }
    });

  }
  selectUsers() {

   

      ;

  }
  selectAll(e) {
    if (e.target.checked) {
      for (let i = 0; i < this.arrUser.length; i++) {
        this.arrUser[i]['isSelect'] = true;
      }
    }
    else {
      for (let i = 0; i < this.arrUser.length; i++) {
        this.arrUser[i]['isSelect'] = false;
      }
    }
  }

  approveGuest() {

    //this.selectedUser = _.filter(this.arrUser, function (element) { return element['isSelect'] == true; });
    //console.log(this.selectedUser);
    let input = {};
    input['data'] = [];
    this.arrUser.forEach(element => {
      let temp = {};
      temp['userId'] = element.userId;
      temp['eventId'] = this.eventId;
      temp['status'] = (element.isSelect == true) ? "2": "3";
      input['data'].push(temp);
    });

    input['condn'] = {eventId:this.eventId};

    console.log(input);
    this.commanService.postData(input, apis.APPRV_GUEST).subscribe(
      data => {
        if (data.result == true) {
          //this.arrUser=data.data;     
          swal(
            'Submited!',
            'Your guest list has been approved successfully.',
            'success'
          )

          this.router.navigate([this.arrRoutes.dashboard]);
        }
      });
  }

}
