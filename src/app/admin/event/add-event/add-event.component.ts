import { Component, OnInit, EventEmitter, Output, ViewChild, NgZone, ElementRef, ChangeDetectorRef } from '@angular/core';
import { MatChipInputEvent } from '@angular/material';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import {apis} from '../../../config/apis';

//services
import { EventService } from '../../../services/web/event.service'
import { GlobalUtility } from '../../../services/web/globalUtility';

//models
import { Events } from '../../../models/event';

//models
import { EditEvents } from '../../../models/event';

//constants
import { routes } from '../../../config/routes';


//plugins
import swal from 'sweetalert2';
import { FlashMessagesService } from 'ngx-flash-messages';


declare var $: any;

import { FormControl } from '@angular/forms';
import { AgmCoreModule, MapsAPILoader } from '@agm/core';
import { } from '@types/googlemaps';
import { } from 'googlemaps';
import { CommanService }  from '../../../services/comman.service';

@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.css']
})

export class AddEventComponent implements OnInit {

  public latitude: number;
  public longitude: number;
  public addInterestForm: FormGroup;
  public searchPlaceControl: FormControl;
  public interesettxt: FormControl;
  public quetion: FormControl;
  public zoom: number;
  autocomplete: any;
  loading: boolean = false;
  visible: boolean = true;
  selectable: boolean = true;
  removable: boolean = true;
  addOnBlur: boolean = true;
  interests = [];
  questionList = ['Question 1', 'Question 2', 'Question 3'];

  separatorKeysCodes = [ENTER, COMMA];
  services = [];
  arrClubs;
  public intersetList = ['Jazz', 'sad', 'gdffd', 'dsfsdf'];

  filteredOptions: Observable<string[]>;

  @ViewChild("location")
  public searchElementRef: ElementRef;

  @Output()
  childEvent = new EventEmitter<string>();

  user_info = JSON.parse(localStorage.getItem('user_info'));
  model = new Events('', this.user_info._id, '', '', '', 0, 0, '', [],'','');
  model1 = new EditEvents('', '','','',0,0,'',[], '','', [], []);
  constructor(private commanService:CommanService,private route: ActivatedRoute, private _router: Router, private eventService: EventService, private flashMessagesService: FlashMessagesService, private utility: GlobalUtility, private mapsAPILoader: MapsAPILoader, private ngZone: NgZone, private ref: ChangeDetectorRef, el: ElementRef) { }


  ngOnInit() {
    this.zoom = 4;
    this.latitude = 39.8282;
    this.longitude = -98.5795;

    //create search FormControl
    this.searchPlaceControl = new FormControl();
    this.interesettxt = new FormControl();
    this.quetion = new FormControl();

    this.addInterestForm = new FormGroup({
      quetion: this.quetion,
      interest: this.interesettxt
    });

    this.filteredOptions = this.interesettxt.valueChanges.pipe(startWith(''), map(val => this.filter(val)));

    this.commanService.postData({"userType":"2"},apis.GET_ALL_USERS).subscribe(
      data => {
        if (data.result==true) {
          this.arrClubs=data.data;     
         
        }
      });


  }
  selectedOption() {
    this.interests.push(this.interesettxt.value);
  }

  filter(val: string): string[] {
    return this.intersetList.filter(option =>
      option.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  /* add custom interst */
  onSubmit() {
    this.interests.push(this.interesettxt.value);
    this.intersetList.push(this.interesettxt.value);
    $('#custominterest').modal('hide');
  }
  /* end add custom interst */

  submitForm(model, formvalid) {
    debugger;
    if (formvalid == true) {

      console.log(this.services);
      let uploadFile = (<HTMLInputElement>window.document.getElementById('eventMedia')).files;
     
      console.log(uploadFile);
      let formData:FormData = new FormData();
      for (var i = 0; i < uploadFile.length; i++) {
        formData.append('eventMedia[]', uploadFile[i]);
        
      }
      
      formData.append('services', JSON.stringify(this.services));

      formData.append('eventName', model.eventName);
      formData.append('description', model.description);
      formData.append('userId', model.userId);
      formData.append('eventDate', model.eventDate);
      formData.append('minAgeLimit', model.minAgeLimit);
      formData.append('maxAgeLimit', model.maxAgeLimit);
      formData.append('location', model.location);
      formData.append('eventType', model.eventType);
      formData.append('clubId', model.clubId);

      formData.append('location', this.searchPlaceControl.value);
      formData.append('eventCoordinate', JSON.stringify([this.longitude, this.latitude]));
      formData.append('interests', JSON.stringify(this.interests));

     

      this.eventService.addEvent(formData).subscribe(
        data => {
          if (data.result) {

            $('#add-event').modal('hide');
            swal('Event added successfully');
            this.getEvents();
           // this.model = new Events('', this.user_info._id, '', '', '', 0, 0, '', [],'','');
          
          }
          else {
            this.model = new Events('', this.user_info._id, '', '', '', 0, 0, '', [],'','');
            this.flashMessagesService.show(data.message, {
              classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
              timeout: 5000, // Default is 3000
            });
           
          }


        },
        error => {
          swal(error);
          this.loading = false;
        });

    }
    else {

      this.flashMessagesService.show("Please all details", {
        classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
        timeout: 5000, // Default is 3000
      });

    }

    // this._router.navigate(['/dashboard']);
    // window.location.reload();
  }


  add(event: MatChipInputEvent): void {
    let input = event.input;
    let value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.services.push({ name: value.trim() });
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(service: any): void {
    let index = this.services.indexOf(service);

    if (index >= 0) {
      this.services.splice(index, 1);
    }
  }
  getEvents() {
    this.childEvent.emit('this is a test');
  }
  changeAge(value, isMin) {
    //debugger;
    let val = parseInt(value);
    //let isMin ='0';
    if (val > this.model['maxAgeLimit'] && isMin == '1') {
      this.model['maxAgeLimit'] = val + 1;
    }
    if (this.model['minAgeLimit'] > this.model['maxAgeLimit'] && isMin == '0') {
      this.model['minAgeLimit'] = val - 1;
    }
  }
  loadmap() {

    this.setCurrentPosition();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      this.autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      this.autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result

          let place: google.maps.places.PlaceResult = this.autocomplete.getPlace();
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          this.searchPlaceControl.setValue(place.formatted_address);

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;

        });
      });
    });
  }

  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }

  /*----end geolocation----*/

}