import { Component, OnInit ,EventEmitter,Output,Input } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
//models
import { Events } from '../../../models/event';

//constants
import {routes} from '../../../config/routes';

@Component({
  selector: 'app-show-event',
  templateUrl: './show-event.component.html',
  styleUrls: ['./show-event.component.css']
})
export class ShowEventComponent implements OnInit {

  
  @Input() objEvent;
  @Input() objServices;

  services;
  locEvent = {}; 
  constructor() { }

  ngOnInit() {
    
  }
  
  getShowEvent() {
    console.log('objServices',this.objServices);
    this.locEvent =  this.objEvent
   
  }

}
