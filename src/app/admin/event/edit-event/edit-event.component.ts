import { Component, OnInit ,EventEmitter,Output,Input,ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import {MatChipInputEvent} from '@angular/material';
import {ENTER, COMMA} from '@angular/cdk/keycodes';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import {apis} from '../../../config/apis';
//services
import { EventService } from  '../../../services/web/event.service'
import { GlobalUtility } from '../../../services/web/globalUtility';
import { CommanService } from '../../../services/comman.service';


//models
import {EditEvents} from '../../../models/event';

//constants
import {routes} from '../../../config/routes';


//plugins
import swal from 'sweetalert2';
import { FlashMessagesService } from 'ngx-flash-messages';

declare var $: any;
var ELEMENT_DATA: Element[] = [];

@Component({
  selector: 'app-edit-event',
  templateUrl: './edit-event.component.html',
  styleUrls: ['./edit-event.component.css']
})
export class EditEventComponent implements OnInit {

  

  @Output()
  childEvent = new EventEmitter<string>();
  

  @Input() objEvent;
  model = new EditEvents('','','','',0,0,'',[],'','',[],[]);


  
  constructor(private commanService: CommanService,private route: ActivatedRoute,private _router: Router,private eventService:EventService,private flashMessagesService:FlashMessagesService,private utility :GlobalUtility) { }

  event_info;
  objUser;
  dataSource;
  loading:boolean = false;
  visible: boolean = true;
  selectable: boolean = true;
  removable: boolean = true;
  addOnBlur: boolean = true;
  arrClubs;
  // Enter, comma
  separatorKeysCodes = [ENTER, COMMA];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  services;

  ngOnInit () {
//this.event_info = JSON.parse(localStorage.getItem('event_info'));
    this.commanService.postData({"userType":"2"},apis.GET_ALL_USERS).subscribe(
      data => {
        if (data.result==true) {
          this.arrClubs=data.data;               
        }
      });

  }

  getChoices(){
    debugger;
    this.childEvent.emit('this is a test');
    }

  submitForm(model, formvalid) {
    debugger;
    if (formvalid == true) {
      let input = {      
        'data':model,
        'condn':{'_id':model._id}        
      };
      this.eventService.updateEvent(input).subscribe(
        data => {
          if (data.result) 
          {
              $('#edit-event').modal('hide');              
              swal('Event updated successfully');
              this.getChoices();
          }
          else {
            this.flashMessagesService.show(data.message, {
              classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
              timeout: 5000, // Default is 3000
            });
          }
        },
        error => {
          swal(error);
          this.loading = false;
        });

    }
    else {

      this.flashMessagesService.show("Please Fill all details", {
        classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
        timeout: 5000, // Default is 3000
      });

    }
  }


  add(event: MatChipInputEvent): void {
    let input = event.input;
    let value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.services.push({ name: value.trim() });
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(service: any): void {
    let index = this.services.indexOf(service);

    if (index >= 0) {
      this.services.splice(index, 1);
    }
  }
 

  getEditEvent() {
    debugger;
    this.model = new EditEvents(this.objEvent._id,
      this.objEvent.eventName,
      this.objEvent.description,
      this.objEvent.eventDate,
      this.objEvent.minAgeLimit,
      this.objEvent.maxAgeLimit,
      this.objEvent.location,
      this.objEvent.services,
      this.objEvent.eventMedia,
      this.objEvent.eventType,
      this.objEvent.comments,
      this.objEvent.attendees 
    
    );
      //   this.event_info = JSON.parse(localStorage.getItem('event_info'));
      //  // console.log(this.event_info);
      //   let services_text:any = '';
      //   //this.model.services = [];
      //   this.services = this.event_info.services;
      //   this.model._id = this.event_info._id;
      //   this.model.eventName = this.event_info.eventName;
      //   this.model.description = this.event_info.description;
      //   this.model.eventDate = this.event_info.eventDate;
      //   this.model.minAgeLimit = this.event_info.minAgeLimit;
      //   this.model.maxAgeLimit = this.event_info.maxAgeLimit;
      //   this.model.location = this.event_info.location;
      //   this.model.eventType = this.event_info.eventType;
      // //  this.model.clubId = this.event_info.clubId;
      //   this.model.comments=[];
      //   this.model.attendees=[];


  }

  getEvents(data = {userId:this.objUser._id}) {
    debugger;
    this.eventService.getEvents(data).subscribe(
      res => {
        if (res.result) {
          ELEMENT_DATA = res.data;
          this.dataSource = new MatTableDataSource<Element>(ELEMENT_DATA);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          let eventData = res.data;
          console.log('res.data1', eventData);
        }
      },
      err => {

      }

    )
  }
}
