import { Component, Output, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { GlobalUtility } from '../../services/web/globalUtility';
import { EventService } from '../../services/web/event.service';
import {routes} from '../../config/routes';
import {ShowEventComponent} from './show-event/show-event.component';
import { FormControl,FormGroup,Validators} from '@angular/forms';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

//plugins
import swal from 'sweetalert2';
import { AddEventComponent } from './add-event/add-event.component';
import { EditEventComponent } from './edit-event/edit-event.component';

export interface Element {
  eventName: string;
  description: number;
  eventDate: number;
  actions: string;
  eventData: any;
  minAgeLimit: number;
}
var ELEMENT_DATA: Element[] = [];
declare var $: any;

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {

  @ViewChild(EditEventComponent ) child: EditEventComponent; 
  @ViewChild(ShowEventComponent ) child2: ShowEventComponent; 
  @ViewChild(AddEventComponent ) child3: AddEventComponent; 

  @Output()
  ParentEvent = new EventEmitter<string>();

  displayedColumns = ['eventName', 'description','location', 'eventDate', 'minAgeLimit', 'actions'];
  dataSource;
  isSpiner = true;
  arrRoutes = routes;
  objEvent:any=[];
  objServices:any=[];
  objUser;
  adduserForm:FormGroup;



  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private eventService: EventService, public utility: GlobalUtility) { }
  ngAfterViewInit() {
    this.objUser = JSON.parse(localStorage.getItem('user_info'));
   // debugger;
    this.getEvents();

  }



  ngOnInit() {
    //console.log(ELEMENT_DATA);
  }
  getEvents(data = {userId:this.objUser._id}) {
    this.eventService.getEvents(data).subscribe(
      res => {
        this.isSpiner = false;
        if (res.result) {
          ELEMENT_DATA = res.data;
         // console.log('res.data', res.data);
          this.dataSource = new MatTableDataSource<Element>(ELEMENT_DATA);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          let eventData = res.data;
          
         // console.log('res.data1', eventData);
        }
      },
      err => {
        this.isSpiner = false;
      }

    )
  }

  editEvent(eventId) {
    let input = {};
    input['_id'] = eventId;
    
    this.eventService.getEvents(input).subscribe(
      res => {
        if (res.result) {
           this.objEvent = res.data;
         //  localStorage.setItem('event_info', JSON.stringify( this.objEvent));
          
          setTimeout(() => {
            $('#edit-event').modal('show');    
            this.child.getEditEvent();
          }, 1000);
          
        }
      },
      err => {

      }
    )
  }

  showEvent(eventId) {
      let input = {};
      input['_id'] = eventId;
      this.eventService.getEvents(input).subscribe(
        res => {
          if (res.result) {
            this.objEvent = JSON.parse(JSON.stringify(res.data));
            this.objServices = this.objEvent.services;
           // console.log("obj",this.objServices);
            $('#show-event').modal('show');
          }
        },
        err => {
        }
      )
  }

  addEvent() {
    this.child3.loadmap();
  }


  deleteEvent(eventId) {

    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Event!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {

        let input = {};
        input['_id'] = eventId;
        //debugger;
        this.eventService.deleteEvent(input).subscribe(
          res => {
            if (res.result) {
              
             this.getEvents({userId:this.objUser._id});
             swal(
              'Deleted!',
              'Your event has been deleted.',
              'success'
            )
            }
          },
          err => {
            swal(
              'Error',
              'Something went wrong :(',
              'error'
            )
          }
        )
       
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal(
          'Cancelled',
          'Your event is safe :)',
          'error'
        )
      }
    })   
  }



  adEvent()
  {
    this.child3.loadmap();
    $('#add-event').modal('show');

  }
  
}
