import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { InboxService } from '../../services/web/inbox.service';
import * as _ from 'lodash';
//plugins
import { FlashMessagesService } from 'ngx-flash-messages';
import swal from 'sweetalert2';

@Component({
  selector: 'app-mailbox',
  templateUrl: './mailbox.component.html',
  styleUrls: ['./mailbox.component.css']
})
export class MailboxComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  arrClubApps :string[] = [];
  clubApplication='';
  constructor(private inboxService:InboxService,private flashMessagesService:FlashMessagesService) { }


  ngOnInit() {
    
    this.getClubApplication();
  }

  getClubApplication() {
    this.inboxService.getClubApplication().subscribe(
      res => { 
        //console.log(res);
        if(res.result)
        {
          this.arrClubApps = res.data;
        
        }else{
          this.flashMessagesService.show(res.message, {
            classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
            timeout: 5000, // Default is 3000
          });
        }
       },err => {
       }
    )
  }

  showApplication(apps) {
    //debugger;
    this.clubApplication = apps;
  }
  acceptApplication(status,clubApplication) {
    let input = {};
    
    input['status'] = status;
    input['type'] = '2';
    input['verifyCode'] = Math.floor(Math.random() * 9999) + 1;
    input['_id'] = clubApplication._id;
    input['email'] = clubApplication.email;
    this.inboxService.acceptClubApplication(input).subscribe(
      res => { 
        //console.log(res);
        
        if(res.result)
        {
          this.clubApplication['status'] = status
          swal('Responded Successfully');
        
        }else{
          swal('Something went wrong');
        }
       },err => {
        
       }
    )
  }

}
