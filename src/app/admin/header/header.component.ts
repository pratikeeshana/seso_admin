import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {routes} from '../../config/routes';
//services
import { GlobalUtility } from '../../services/web/globalUtility';
declare var $ :any;
declare const layout:any;


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  arrRoutes = routes;
 
  constructor(private _router: Router,public utility :GlobalUtility) { }
  user_info;
  ngOnInit() {

    this.user_info = JSON.parse(localStorage.getItem('user_info'));
    console.log(this.user_info.userType);
  }

  logout(){
    //localStorage.setItem('user_info','');
    localStorage.clear();
    this._router.navigate([this.arrRoutes.login]);
    $("body").removeClass('color-purple').removeClass('color-default').removeClass('color-primary').removeClass('color-red').removeClass('color-green').removeClass('color-orange').removeClass('color-blue');
    $("body").addClass('color-purple');
  }

}
