import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup,Validators} from '@angular/forms';
import { AuthService } from '../../services/web/auth.service';
import { CommanService } from '../../services/comman.service';
import { FlashMessagesService } from 'ngx-flash-messages';

import {apis} from '../../config/apis';

//plugins
import swal from 'sweetalert2';
declare var $: any;

//Model
import { Users } from '../../models/user';
import { nearer } from 'q';
import { UserlistComponent } from '../userlist/userlist.component';


@Component({
  selector: 'app-subuser',
  templateUrl: './subuser.component.html',
  styleUrls: ['./subuser.component.css']
})
export class SubuserComponent implements OnInit {

  showLoader = true;
  adduserForm:FormGroup;
  email:FormControl;
	password:FormControl;
	userName:FormControl;
	dob:FormControl;
	userType:FormControl;
	contact:FormControl;
  city:FormControl;
  state:FormControl;
  zipCode:FormControl;
  parentPartnerId:FormControl;
  subuserId:any;
  showDetails:boolean=false;
  user_info = JSON.parse(localStorage.getItem('user_info'));
  subuserDetail = {};
  subuserDetails={
    "email":"ash@gmail.com",
    "userName":"ash",
    "dob":"2018-05-15",
    "contact":8888888888,
    "city":"Pune",
    "state":"Maharashtra",
    "zipCode":"411043",
    "access":{
      "view":true,
      "edit":true,
      "delete":false
    }
  };

  model = new Users('','',0,'','','','','','','','2','','','',0,'','','',[],'');


  constructor(private commanService:CommanService,
              private objAuthServive:AuthService,
              private flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
    this.createForm();
    let data = {};

    if(this.user_info)
    {
      data['parentPartnerId'] = this.user_info._id;
      this.commanService.postData(data,apis.GET_ALL_USERS).subscribe(
        res => {
          this.showLoader = false;
          this.subuserDetail = (res.data[0]) ? res.data[0]: {};
          console.log('this.subuser', this.subuserDetail);
          if(res.data.length > 0)
            this.showDetails = true;         
        },
        err => {

        }
      )
    }
  }

  getUser(){
    debugger;
    this.createForm();
    let data = {};

    if(this.user_info)
    {
      data['parentPartnerId'] = this.user_info._id;
      this.commanService.postData(data,apis.GET_ALL_USERS).subscribe(
        res => {
          this.showLoader = false;
          this.subuserDetail = (res.data[0]) ? res.data[0]: {};
          console.log('this.subuser', this.subuserDetail);
          if(res.data.length > 0)
            this.showDetails = true;         
        },
        err => {

        }
      )
    }
  }

  createForm()
  {
    this.adduserForm = new FormGroup({
      parentPartnerId :new FormControl(this.user_info._id),
      email : new FormControl('',[Validators.required, Validators.pattern("[^ @]*@[^ @]*") ]),
     // password : new FormControl('',Validators.required),
      userName : new FormControl('',Validators.required),
      dob : new FormControl('',Validators.required),
      userType : new FormControl(6),
      contact : new FormControl('',[Validators.required,Validators.pattern("[0-9]+")]),
      city : new FormControl('',Validators.required),
      state : new FormControl('',Validators.required),
      zipCode : new FormControl('',[Validators.required,Validators.pattern("[0-9]+")]),
    });
  }

  showDetail()
  {
    this.showDetails=true;
  }

  hideDetails()
  {
    this.showDetails=true;
  }

  
//   onSubmit(model){
//     debugger;
//     console.log(this.model.dob);
//     this.objAuthServive.register(this.model).subscribe(
//       objResponse => {
//         if(objResponse.result){        
//           this.showDetails=true;
//           this.getUser();
//           swal('User added successfully');     
//         }
//         else {
//           this.flashMessagesService.show(objResponse.message, {
//             classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
//             timeout: 5000, // Default is 3000
//           });
         
//         }
//       },
//       error => {
//         swal(error);
//         this.showLoader = false;
//       });
//  }

  // add subuser
  onSubmit()
  {
    debugger;
    if(this.adduserForm.valid)
    {

      if(this.subuserDetail == {})
      {
        this.objAuthServive.register(this.adduserForm.value).subscribe(
          data => {
            if (data.result==true) {
              console.log(data._id);
              this.subuserId=data._id;
              this.showDetails=true;
            }
          });
      }
      else
      {
        let data = {};
        data['data'] = this.adduserForm.value;
        data['condn'] = { _id: this.subuserDetail['_id']}
        this.commanService.postData(data,apis.UPDATE_USER).subscribe(
          res => {
            this.showLoader = false ;
            this.getUser();         
           
          },
          err => {
  
          }
        )
      }
    }
  }


  editUser(userId) {
    this.adduserForm = new FormGroup({
      parentPartnerId :new FormControl(this.user_info._id),
      email : new FormControl(this.subuserDetail['email'],[Validators.required, Validators.pattern("[^ @]*@[^ @]*") ]),
      //password : new FormControl(this.subuserDetailpassword,Validators.required),
      userName : new FormControl(this.subuserDetail['userName'],Validators.required),
      dob : new FormControl(this.subuserDetail['dob'],Validators.required),
      userType : new FormControl(6),
      contact : new FormControl(this.subuserDetail['contact'],[Validators.required,Validators.pattern("[0-9]+"), Validators.maxLength(10),  Validators.minLength(10) ]),
      city : new FormControl(this.subuserDetail['city'],Validators.required),
      state : new FormControl(this.subuserDetail['state'],Validators.required),
      zipCode : new FormControl(this.subuserDetail['zipCode'],[Validators.required,Validators.pattern("[0-9]+")]),
    });
    this.showDetails = false;
  }


  deleteUser(uId) {
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Event!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {

        let input = {};
        input['userId'] = uId;
        this.commanService.deleteData(input).subscribe(
          res => {
            if (res.result) {
            
             swal(
              'Deleted!',
              'Your event has been deleted.',
              'success'
            )
            this.showDetails = false;
            // this.getUser();
            }
          },
          err => {
            swal(
              'Error',
              'Something went wrong :(',
              'error'
            )
          }
        )
       
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal(
          'Cancelled',
          'Your event is safe :)',
          'error'
        )
      }
    })   
  }


}