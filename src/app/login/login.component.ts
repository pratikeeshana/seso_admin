import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';


import { Routes, RouterModule, RouterLinkActive } from '@angular/router';
import { Router } from '@angular/router';

import { Users } from '../models/user'; // model

//services
import { AuthService } from '../services/web/auth.service';

//plugins
import swal from 'sweetalert2';
import { FlashMessagesService } from 'ngx-flash-messages';

//constants
import {routes} from '../config/routes';

@Component({
  selector: 'app-root',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private _router: Router, private authService: AuthService, private flashMessagesService: FlashMessagesService) { 
   
  }

  model = new Users('','', 0, '','', '', '', '', '','', '', '', '', '', 0, '', '', '', {},'')
  public loading = false;
  ngOnInit() {
   
  }
  submitForm(model, formvalid) {
    if (formvalid == true) {
      let input = {};
      input['email'] = model.email;
      input['password'] = model.password;
      input['userType'] = model.userType;
      this.authService.login(input).subscribe(

        data => {


          if (data.result) {
            localStorage.setItem('user_info', JSON.stringify(data.data));
            this._router.navigate([routes.dashboard]);
          }
          else {
            this.flashMessagesService.show(data.message, {
              classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
              timeout: 5000, // Default is 3000
            });
          }


        },
        error => {
          swal(error);
          this.loading = false;
        });

    }
    else {

      this.flashMessagesService.show("Please all details", {
        classes: ['alert', 'alert-danger'], // You can pass as many classes as you need
        timeout: 5000, // Default is 3000
      });

    }

    // this._router.navigate(['/dashboard']);
    // window.location.reload();
  }

}
