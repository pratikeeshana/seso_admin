export class Events {
    constructor(

        public _id: String,
        public userId: String,
        public eventName: String,
        public description: String,
        public eventDate: String,
        public minAgeLimit: Number,
        public maxAgeLimit: Number,
        public location:String,
        public services: object,
        public eventMedia:String,
        public eventType:String
    ) { }
}

export class EditEvents {
    constructor(

        public _id: String,
        public eventName: String,
        public description: String,
        public eventDate: String,
        public minAgeLimit: Number,
        public maxAgeLimit: Number,
        public location:String,
        public services: object,
        public eventMedia:String,
        public eventType:String,
        public comments:object,
        public attendees:object



    ) { }
}