
export class Users {
    constructor(
        public fullName: String,
         public user_id: String,
         public case_id: number,
         public userName: string,
         public email: string,
         public password: string,
         public confirmNewPassword:string,
         public dob:string,
         public verifyCode:string,
         public contact:string,
         public userType:string,
         public city:string,
         public state:string,
         public relStatus:string,
         public zipCode:Number,
         public schoolInfo:string,
         public workInfo:string,
         public aboutYou:string,
         public socialLinks:object,
         public venueTheme:string
         
      
     ){}
}