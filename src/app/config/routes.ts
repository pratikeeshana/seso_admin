export var routes = {

    base :'/',
    home :'/home',
    terms : '/terms',
    login : '/login',
    register : '/register/:code',
    dashboard : '/admin/dashboard',
    mailbox : '/admin/mailbox',
    event : '/admin/event',
    account_setting :  '/admin/account-setting',
    profile_setting :  '/admin/profile-setting',
    media :  '/admin/media',
    choice :  '/admin/choice',
    invite_promoter : '/admin/invite-promoter',
    promotors : '/admin/promotors',
    followers : '/admin/followers',
    followings : '/admin/followings',
    subuser :  '/admin/subuser',
    userlist :  '/admin/userlist',
    feedback :  '/admin/feedback',
    guestlist: '/admin/guestlist',
    addpramotor: '/admin/addpramotor',
    demo: '/admin/demo'
  

}